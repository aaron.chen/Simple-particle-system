/*
RandomSphere class
Information about sphere:
radius, material colors, shininess position, and velocity
*/

//RandomSphere method - new RandomSphere
//Constructor generating random parameters for sphere
var RandomSphere = function () {
	this.radius = Math.random()*0.11 + 0.05;
		
	this.dif_r = Math.random();
	this.dif_g = Math.random();
	this.dif_b = Math.random();

	this.amb_r = Math.random();
	this.amb_g = Math.random();
	this.amb_b = Math.random();
	
	this.spe_r = Math.random();
	this.spe_g = Math.random();
	this.spe_b = Math.random();

	this.shininess = Math.random()*200;

	this.pos = vec3.fromValues(Math.random() - 0.5,
							Math.random() - 0.5,
							Math.random() - 0.5);
	
	this.x_vel = DEFAULT_SPEED_SCALE*(Math.random() * 2 - 1);
	this.y_vel = DEFAULT_SPEED_SCALE*(Math.random() * 2 - 1);
	this.z_vel = DEFAULT_SPEED_SCALE*(Math.random() * 2 - 1);
}

//RandomSphere method - get_dif()
//Return diffuse material color parameters as array to use in uploadMaterialToShader()
RandomSphere.prototype.get_dif = function () {
	return [this.dif_r, this.dif_g, this.dif_b];
}

//RandomSphere method - get_amb()
//Return ambient material color parameters as array to use in uploadMaterialToShader()
RandomSphere.prototype.get_amb = function() {
	return [this.amb_r, this.amb_g, this.amb_b];
}

//RandomSphere method - get_spe()
//Return specular material color parameters as array to use in uploadMaterialToShader()
RandomSphere.prototype.get_spe = function() {
	return [this.spe_r, this.spe_g, this.spe_b];
}

//RandomSphere method - update_pos()
//Update current position of RandomSphere using a custom time to update the position from its velocity (Euler integration)
//Also uses custom drag and collision with box to update velocity
//Uses default gravity
RandomSphere.prototype.update_pos = function(dt, drag) {
	//reverse x velocity if ball x-side hits wall
	if (this.pos[0] + this.radius >= 1)
		this.x_vel = -Math.abs(this.x_vel);
	else if  (this.pos[0] - this.radius <= -1){
		this.x_vel = Math.abs(this.x_vel);
	}

	//reverse y velocity if ball y-side hits wall
	if (this.pos[1] + this.radius >= 1)
		this.y_vel = -Math.abs(this.y_vel);
	else if  (this.pos[1] - this.radius <= -1){
		this.y_vel = Math.abs(this.y_vel);
	}
	
	//reverse z velocity if ball y-side hits wall
	if (this.pos[2] + this.radius >= 1)
		this.z_vel = -Math.abs(this.z_vel);
	else if  (this.pos[2] - this.radius <= -1){
		this.z_vel = Math.abs(this.z_vel);
	}

	//update velocity with drag
	this.x_vel *= Math.pow(drag,dt);
	this.y_vel *= Math.pow(drag,dt);
	this.z_vel *= Math.pow(drag,dt);

	//make ball stop bouncing once low enough and not high enough y velocity
	if (Math.abs(this.y_vel) < 0.0001 && this.pos[1] - this.radius <= -0.999){
		this.y_vel = 0;
	}
	else{
		//otherwise update y-velocity using gravity (euler integration)
		this.y_vel += DEFAULT_GRAVITY * dt;
	}

	//move the position vector by adding velocity * dt
	var velVec = vec3.fromValues(this.x_vel, this.y_vel, this.z_vel);
	vec3.scale(velVec, velVec, dt)
	vec3.add(this.pos, this.pos, velVec);

	//manually move ball above floor if now outside the box
	if (this.pos[1] - this.radius < -1){
		this.pos[1] = -1.0 + this.radius;
		this.y_vel = Math.abs(this.y_vel);
	}
}

//array to hold our spheres
var spheres = [];
//affects the max speed our random balls can start with
var DEFAULT_SPEED_SCALE = 3.0;
//acceleration downward (y-axis) on balls
var DEFAULT_GRAVITY = -9.81;
//speed dampening on balls
var DEFAULT_DRAG = 0.9;
//last time the balls were drawn so we can measure dt using real time
var last_draw;

var gl;
var canvas;
var shaderProgram;

// Create a place to store sphere geometry
var sphereVertexPositionBuffer;

//Create a place to store sphere normals for shading
var sphereVertexNormalBuffer;

// Create a place to store floor geometry
var floorVertexPositionBuffer;

//Create a place to floor normals for shading
var floorVertexNormalBuffer;

// View parameters
var eyePt = vec3.fromValues(0.0,0.0,3.0);
var viewDir = vec3.fromValues(0.0,0.0,-1.0);
var up = vec3.fromValues(0.0,1.0,0.0);
var viewPt = vec3.fromValues(0.0,0.0,0.0);

// Create the Normal Matrix
var nMatrix = mat3.create();

// Create ModelView matrix
var mvMatrix = mat4.create();

//Create Projection matrix
var pMatrix = mat4.create();

var mvMatrixStack = [];

//Held down keys for interactivity
var currentlyPressedKeys = {};

//rotation/pitch speed parameters
var DEFAULT_ROT_SPEED = 0.025;
var DEFAULT_PIT_SPEED = 0.025;

//rolling parameters + relevant quaternions
var rotationSpeed = DEFAULT_ROT_SPEED;
var rotationQuatLeft = quat.create();
var rotationQuatRight = quat.create();

//pitching parameters + relevant quaternions
var pitchSpeed = DEFAULT_PIT_SPEED;
var rotationQuatUp = quat.create();
var rotationQuatDown = quat.create();
var sidewaysVec = vec3.create();

//-------------------------------------------------------------------------
/**
 * Sends Modelview matrix to shader
 */
function uploadModelViewMatrixToShader() {
	gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

//-------------------------------------------------------------------------
/**
 * Sends projection matrix to shader
 */
function uploadProjectionMatrixToShader() {
	gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, 
											false, pMatrix);
}

//-------------------------------------------------------------------------
/**
 * Generates and sends the normal matrix to the shader
 */
function uploadNormalMatrixToShader() {
	mat3.fromMat4(nMatrix,mvMatrix);
	mat3.transpose(nMatrix,nMatrix);
	mat3.invert(nMatrix,nMatrix);
	gl.uniformMatrix3fv(shaderProgram.nMatrixUniform, false, nMatrix);
}

//----------------------------------------------------------------------------------
/**
 * Pushes matrix onto modelview matrix stack
 */
function mvPushMatrix() {
	var copy = mat4.clone(mvMatrix);
	mvMatrixStack.push(copy);
}

//----------------------------------------------------------------------------------
/**
 * Pops matrix off of modelview matrix stack
 */
function mvPopMatrix() {
	if (mvMatrixStack.length == 0) {
		throw "Invalid popMatrix!";
	}
	mvMatrix = mvMatrixStack.pop();
}

//----------------------------------------------------------------------------------
/**
 * Sends projection/modelview matrices to shader
 */
function setMatrixUniforms() {
	uploadModelViewMatrixToShader();
	uploadNormalMatrixToShader();
	uploadProjectionMatrixToShader();
}

//----------------------------------------------------------------------------------
/**
 * Translates degrees to radians
 * @param {Number} degrees Degree input to function
 * @return {Number} The radians that correspond to the degree input
 */
function degToRad(degrees) {
	return degrees * Math.PI / 180;
}

//----------------------------------------------------------------------------------
/**
 * Creates a context for WebGL
 * @param {element} canvas WebGL canvas
 * @return {Object} WebGL context
 */
function createGLContext(canvas) {
	var names = ["webgl", "experimental-webgl"];
	var context = null;
	for (var i=0; i < names.length; i++) {
		try {
			context = canvas.getContext(names[i]);
		} catch(e) {}
		if (context) {
			break;
		}
	}
	if (context) {
		context.viewportWidth = canvas.width;
		context.viewportHeight = canvas.height;
	} else {
		alert("Failed to create WebGL context!");
	}
	return context;
}

//----------------------------------------------------------------------------------
/**
 * Loads Shaders
 * @param {string} id ID string for shader to load. Either vertex shader/fragment shader
 */
function loadShaderFromDOM(id) {
	var shaderScript = document.getElementById(id);
	
	// If we don't find an element with the specified id
	// we do an early exit 
	if (!shaderScript) {
		return null;
	}
	
	// Loop through the children for the found DOM element and
	// build up the shader source code as a string
	var shaderSource = "";
	var currentChild = shaderScript.firstChild;
	while (currentChild) {
		if (currentChild.nodeType == 3) { // 3 corresponds to TEXT_NODE
			shaderSource += currentChild.textContent;
		}
		currentChild = currentChild.nextSibling;
	}
 
	var shader;
	if (shaderScript.type == "x-shader/x-fragment") {
		shader = gl.createShader(gl.FRAGMENT_SHADER);
	} else if (shaderScript.type == "x-shader/x-vertex") {
		shader = gl.createShader(gl.VERTEX_SHADER);
	} else {
		return null;
	}
 
	gl.shaderSource(shader, shaderSource);
	gl.compileShader(shader);
 
	if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
		alert(gl.getShaderInfoLog(shader));
		return null;
	} 
	return shader;
}

//----------------------------------------------------------------------------------
/**
 * Setup the fragment and vertex shaders
 */
function setupShaders() {
	vertexShader = loadShaderFromDOM("blinnphongshader-vs");
	fragmentShader = loadShaderFromDOM("blinnphongshader-fs");
	
	shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
		alert("Failed to setup shaders");
	}

	gl.useProgram(shaderProgram);

	shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
	gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

	shaderProgram.vertexNormalAttribute = gl.getAttribLocation(shaderProgram, "aVertexNormal");
	gl.enableVertexAttribArray(shaderProgram.vertexNormalAttribute);

	shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
	shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
	shaderProgram.nMatrixUniform = gl.getUniformLocation(shaderProgram, "uNMatrix");
	shaderProgram.uniformLightPositionLoc = gl.getUniformLocation(shaderProgram, "uLightPosition");    
	shaderProgram.uniformAmbientLightColorLoc = gl.getUniformLocation(shaderProgram, "uAmbientLightColor");  
	shaderProgram.uniformDiffuseLightColorLoc = gl.getUniformLocation(shaderProgram, "uDiffuseLightColor");
	shaderProgram.uniformSpecularLightColorLoc = gl.getUniformLocation(shaderProgram, "uSpecularLightColor");
	shaderProgram.uniformDiffuseMaterialColor = gl.getUniformLocation(shaderProgram, "uDiffuseMaterialColor");
	shaderProgram.uniformAmbientMaterialColor = gl.getUniformLocation(shaderProgram, "uAmbientMaterialColor");
	shaderProgram.uniformSpecularMaterialColor = gl.getUniformLocation(shaderProgram, "uSpecularMaterialColor");

	shaderProgram.uniformShininess = gl.getUniformLocation(shaderProgram, "uShininess");    
}

//-------------------------------------------------------------------------
/**
 * Sends material information to the shader
 * @param {Float32Array} a diffuse material color
 * @param {Float32Array} a ambient material color
 * @param {Float32Array} a specular material color 
 * @param {Float32} the shininess exponent for Phong illumination
 */
function uploadMaterialToShader(dcolor, acolor, scolor,shiny) {
  gl.uniform3fv(shaderProgram.uniformDiffuseMaterialColor, dcolor);
  gl.uniform3fv(shaderProgram.uniformAmbientMaterialColor, acolor);
  gl.uniform3fv(shaderProgram.uniformSpecularMaterialColor, scolor);
    
  gl.uniform1f(shaderProgram.uniformShininess, shiny);
}

//-------------------------------------------------------------------------
/**
 * Sends light information to the shader
 * @param {Float32Array} loc Location of light source
 * @param {Float32Array} a Ambient light strength
 * @param {Float32Array} d Diffuse light strength
 * @param {Float32Array} s Specular light strength
 */
function uploadLightsToShader(loc,a,d,s) {
	gl.uniform3fv(shaderProgram.uniformLightPositionLoc, loc);
	gl.uniform3fv(shaderProgram.uniformAmbientLightColorLoc, a);
	gl.uniform3fv(shaderProgram.uniformDiffuseLightColorLoc, d);
	gl.uniform3fv(shaderProgram.uniformSpecularLightColorLoc, s);
}

//----------------------------------------------------------------------------------
/**
 * Populate buffers with data
 */
function setupBuffers() {
	setupSphereBuffers();
	setupFloorBuffers();
}

//-------------------------------------------------------------------------
/**
 * Populates buffers with data for the floor
 */
function setupFloorBuffers() {
	var floorVertices = [
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,

		1.0, -1.0, -1.0,
		1.0, -1.0, 1.0,
		-1.0, -1.0, 1.0,
	];

	// put floor vertices in floor vertex buffer 
	floorVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, floorVertexPositionBuffer);      
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(floorVertices), gl.STATIC_DRAW);
	floorVertexPositionBuffer.itemSize = 3;
	floorVertexPositionBuffer.numItems = 6;
}

//-------------------------------------------------------------------------
/**
 * Draws a floor from the floor buffer
 */
function drawFloor() {
	gl.bindBuffer(gl.ARRAY_BUFFER, floorVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, floorVertexPositionBuffer.itemSize, 
						gl.FLOAT, false, 0, 0);

	setMatrixUniforms();
	gl.drawArrays(gl.TRIANGLES, 0, floorVertexPositionBuffer.numItems);
}

//-------------------------------------------------------------------------
/**
 * Populates buffers with data for spheres
 */
function setupSphereBuffers() {
	var sphereSoup=[];
	var sphereNormals=[];
	var numT = sphereFromSubdivision(6, sphereSoup, sphereNormals);
	console.log("Generated ", numT, " triangles"); 
	sphereVertexPositionBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexPositionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(sphereSoup), gl.STATIC_DRAW);
	sphereVertexPositionBuffer.itemSize = 3;
	sphereVertexPositionBuffer.numItems = numT * 3;
	console.log(sphereSoup.length / 9);

	// Specify normals to be able to do lighting calculations
	sphereVertexNormalBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexNormalBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(sphereNormals),
					gl.STATIC_DRAW);
	sphereVertexNormalBuffer.itemSize = 3;
	sphereVertexNormalBuffer.numItems = numT * 3;
	
	console.log("Normals ", sphereNormals.length / 3);     
}

//-------------------------------------------------------------------------
/**
 * Draws a sphere from the sphere buffer
 */
function drawSphere() {
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexPositionBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, sphereVertexPositionBuffer.itemSize, 
						gl.FLOAT, false, 0, 0);

	// Bind normal buffer
	gl.bindBuffer(gl.ARRAY_BUFFER, sphereVertexNormalBuffer);
	gl.vertexAttribPointer(shaderProgram.vertexNormalAttribute, 
						sphereVertexNormalBuffer.itemSize,
						gl.FLOAT, false, 0, 0);

	setMatrixUniforms();
	gl.drawArrays(gl.TRIANGLES, 0, sphereVertexPositionBuffer.numItems);
}

//----------------------------------------------------------------------------------
/**
 * Draw call that applies matrix transformations to model and draws model in frame
 */
function draw() { 
	var transformVec = vec3.create();
	var lightVec = vec3.fromValues(1,1,1);

	gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);;
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// We'll use perspective 
	mat4.perspective(pMatrix, degToRad(45), gl.viewportWidth / gl.viewportHeight, 0.1, 200.0);

	// We want to look down -z, so create a lookat point in that direction    
	vec3.add(viewPt, eyePt, viewDir);
	// Then generate the lookat matrix and initialize the MV matrix to that view
	mat4.lookAt(mvMatrix,eyePt,viewPt,up);    
	mvPushMatrix();
	vec3.transformMat4(lightVec, lightVec, mvMatrix)
	//Use some boring white light
	uploadLightsToShader([lightVec[0],lightVec[1],lightVec[2]],[0.5,0.5,0.5],[1.0,1.0,1.0],[0.0,0.0,0.0]);

	//draw all spheres
	for (var i = 0; i < spheres.length; i++){
		mvPushMatrix();

		var s = spheres[i];

		//move the sphere to correct place and scale it according to it's held information about its position and radius
		mat4.translate(mvMatrix, mvMatrix, s.pos);
		vec3.set(transformVec, s.radius, s.radius, s.radius);
		mat4.scale(mvMatrix, mvMatrix, transformVec);

		//upload the sphere's material colors
		uploadMaterialToShader(s.get_dif(), s.get_amb(), s.get_spe(), s.shininess);
		//draw it
		drawSphere();

		mvPopMatrix();
	}

	//make the floor blue
	uploadMaterialToShader([0.5,0.5,0.5],[0.0,0.7,0.9],[0.5,0.5,0.5], 88);
	//draw the floor
	drawFloor();
	mvPopMatrix();
}

//----------------------------------------------------------------------------------
/**
 * Animation to be called from tick. Updates globals and performs animation for each tick.
 */
function animate() {
	//calculate dt from the last time animate() was called
	var dt = (Date.now() - last_draw) / 1000.0;
	last_draw = Date.now();
	//update the position of each sphere using dt and default_drag
	for (var i = 0; i < spheres.length; i++){
		spheres[i].update_pos(dt, DEFAULT_DRAG);
	}
}

//user interaction
function handleKeys(){
	// left key - rotate view parameters left around center
	if (currentlyPressedKeys[37]){
		quat.setAxisAngle(rotationQuatLeft, up, -rotationSpeed);
		vec3.transformQuat(eyePt, eyePt, rotationQuatLeft);
		vec3.transformQuat(viewDir, viewDir, rotationQuatLeft);
	}
	// right key - rotate view parameters right around center
	if (currentlyPressedKeys[39]){
		quat.setAxisAngle(rotationQuatRight, up, rotationSpeed);
		vec3.transformQuat(eyePt, eyePt, rotationQuatRight);
		vec3.transformQuat(viewDir, viewDir, rotationQuatRight);
	}
	// up key - pitch camera over center
	if (currentlyPressedKeys[38]){
		//generate third orthogonal vector (sideways)
		//reset quat to rotate relative to sideways vector
		//apply to up and viewDir
		vec3.cross(sidewaysVec, up, viewDir);
		quat.setAxisAngle(rotationQuatUp, sidewaysVec, rotationSpeed);

		vec3.transformQuat(up, up, rotationQuatUp);
		vec3.transformQuat(eyePt, eyePt, rotationQuatUp);
		vec3.transformQuat(viewDir, viewDir, rotationQuatUp);
	}
	// down key - pitch camera under center
	if (currentlyPressedKeys[40]){
		//generate third orthogonal vector (sideways)
		//reset quat to rotate relative to sideways vector
		//apply to up and viewDir
		vec3.cross(sidewaysVec, up, viewDir);
		quat.setAxisAngle(rotationQuatDown, sidewaysVec, -rotationSpeed);

		vec3.transformQuat(up, up, rotationQuatDown);
		vec3.transformQuat(eyePt, eyePt, rotationQuatDown);
		vec3.transformQuat(viewDir, viewDir, rotationQuatDown);
	}
	//c key - clear all the balls
	if (currentlyPressedKeys[67]){
		clearBalls();
	}
	//r key - reset the camera
	if (currentlyPressedKeys[82]){
		resetCamera();
	}

	//q key - completely reset (clear balls and reset camera)
	if (currentlyPressedKeys[81]){
		clearBalls();
		resetCamera();
	}
}

//add two random balls
function addBalls(){
	spheres.push(new RandomSphere());
	spheres.push(new RandomSphere());
}
 
//set inputted key to be considered pressed down
function handleKeyDown(event) {
	currentlyPressedKeys[event.keyCode] = true;
}

//set inputted key to be considered not pressed down
function handleKeyUp(event) {
	currentlyPressedKeys[event.keyCode] = false;
}

//remove all balls, and "free" them from memory
function clearBalls(){
	for (var i = 0; i < spheres.length; i++){
		spheres[i] = null;
	}
	spheres = [];
}

//reset camera to default parameters, also reset rotation/pitch speed params if changed
function resetCamera() {
	// View parameters
	eyePt = vec3.fromValues(0.0,0.0,3.0);
	viewDir = vec3.fromValues(0.0,0.0,-1.0);
	up = vec3.fromValues(0.0,1.0,0.0);
	viewPt = vec3.fromValues(0.0,0.0,0.0);

	rotationSpeed = DEFAULT_ROT_SPEED;
	pitchSpeed = DEFAULT_PIT_SPEED;
}

//----------------------------------------------------------------------------------
/**
 * Startup function called from html code to start program.
*/
function startup() {
	canvas = document.getElementById("myGLCanvas");
	document.onkeydown = handleKeyDown;
	document.onkeyup = handleKeyUp;
	gl = createGLContext(canvas);
	setupShaders();
	setupBuffers();
	gl.clearColor(0.0, 0.0, 0.0, 1.0); //black background
	gl.enable(gl.DEPTH_TEST);

	//start off with 4 spheres
	for (var i = 0; i < 4; i++){
		spheres.push(new RandomSphere());
	}

	//initial time to use for dt
	last_draw = Date.now();
	//start drawing / animating
	tick();
}

//----------------------------------------------------------------------------------
/**
 * Tick called for every animation frame.
 */
function tick() {
	requestAnimFrame(tick);
	draw();

	handleKeys();
	animate();
}

