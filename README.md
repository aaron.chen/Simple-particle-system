# Simple-Particle-System
Simulates a simple particle system (balls) using random bouncing particles regulated by gravity. Motion is updated using Euler integration, and particles slow down with a drag constant.

Particles do not detect collision with each other, and do not update properly if the program is off-screen.

To run, simply run mp4.html in a web browser. Controls are listed in the html page.

![particle-animation](/sample/particle-animation.gif)
